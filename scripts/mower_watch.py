
#!/usr/bin/env python

# 2023 Per Carlen
#

import os
import time
import json
import yaml
import paho.mqtt.client as paho
import syslog
import requests
from typing import Dict
from datetime import datetime
import pytz
from datetime import date
import calendar
import binascii
import math
import time
import subprocess
import copy

batt_level_list = []

def read_config_file(filename: str) -> Dict:
  config = ""
  filename = "mower_watch.yaml"
  path = "/etc/mower/"
  if os.path.isfile(path+filename):
    filename = path + filename
  print("Reading from:",filename)
  try:
    with open(filename,'r') as file:
      config = yaml.safe_load(file)
  except:
    print("ERROR: No config file - ",filename)
    exit(1)
  return config

def mqtt_on_subscribe(client, userdata, mid, granted_qos):
    print("MQTT subscribing")
    pass


def mqtt_on_connect(client, userdata, flags, rc):
    global mqtt_client2

    if rc != 0:
      print("MQTT connection problem")
      client.connected_flag=False
    else:
      print("MQTT client connected:" + str(client))
      mqtt_client2.subscribe("robot_state/json",2)
      mqtt_client2.subscribe("sensors/om_v_charge/data",2)
      client.connected_flag=True


def mower_enter_emergency():
    data = {"Emergency" : 1}
    response = requests.post(config['mower']['url_api'] + "/api/openmower/call/emergency", json = data)
    time.sleep(2)

def mower_exit_emergency():
    data = {"Emergency" : 0}
    response = requests.post(config['mower']['url_api'] + "/api/openmower/call/emergency", json = data)
    time.sleep(2)

def mower_continue():
    global config

    #Continue by first entering, leaving emergency and then continue 
    data = {"Emergency" : 1}
    response = requests.post(config['mower']['url_api'] + "/api/openmower/call/emergency", json = data)
    data = {"Emergency" : 0}
    response = requests.post(config['mower']['url_api'] + "/api/openmower/call/emergency", json = data)

    data = {"Config":{"Bools":[{"Name":"manual_pause_mowing","Value":False}]}}
    response = requests.post(config['mower']['url_api'] + "/api/openmower/call/mower_logic", json = data)

    data = { "Command" : 1 }
    response = requests.post(config['mower']['url_api'] + "/api/openmower/call/high_level_control", json = data)

    print("Order: Continue")
    print("Status Code", response.status_code)
    print("JSON Response ", response.json())
    time.sleep(2)


def mower_dock(current_state):
    global config

    #Can't go to docking from idle mode, without passing mowing

    if current_state != "DOCKING":
      # Continue
      data = {"Config":{"Bools":[{"Name":"manual_pause_mowing","Value":False}]}}
      response = requests.post(config['mower']['url_api'] + "/api/openmower/call/mower_logic", json = data)
      data = { "Command" : 1 }
      response = requests.post(config['mower']['url_api'] + "/api/openmower/call/high_level_control", json = data)

      print("Status Code", response.status_code)
      print("JSON Response ", response.json())
      time.sleep(2)

      # Home
      data = { "Command" : 2 }
      response = requests.post(config['mower']['url_api'] + "/api/openmower/call/high_level_control", json = data)

      print("Order: Dock")
      print("Status Code", response.status_code)
      print("JSON Response ", response.json())
      time.sleep(2)

def mowing_time() -> bool:
    global config

    ret = False
    now = datetime.now(pytz.timezone(config['time_zone']))
    ts = int(now.strftime("%S")) + 60 * int(now.strftime("%M")) + 3600 * int(now.strftime("%H"))
    day_of_week = calendar.day_name[date.today().weekday()][0:3]
    if config['time_control']:
      for daily_schedule in config['time_control']:
        if daily_schedule['day'] == day_of_week:
          start = daily_schedule['start_time']
          end = daily_schedule['end_time']
          start_time = int(start[0:2]) * 3600 + int(start[3:5]) * 60 
          end_time = int(end[0:2]) * 3600 + int(end[3:5]) * 60 
          if start_time > end_time: # passes midnight
            if ts > 0 and ts < start_time : # after midnight?
              start_time = 0
            elif ts < 24*3600 and ts > start_time: # before midnight
              end_time = 24*3600 - 1
          if start_time < ts and end_time > ts:
            ret = True
    return ret


def check_mower():
    global keep_on_running
    global config
    global previous_state
    global v_charge
    global state_json
    global mqtt_client_pub
    global last_pos
    global batt_level_list

    timestamp_now = int(time.time())

    try:
      batt_charge = float(v_charge)
    except:
      return

    batt_level = round(100 * state_json['battery_percentage'],1)
    batt_level_avg = 0

    if batt_level > -200 and batt_level < 200:
      batt_level_list.append(batt_level)
      if len(batt_level_list) > 10:
        batt_level_list.pop(0) 
    if len(batt_level_list) > 0:
      for i in range(len(batt_level_list)):
        batt_level_avg += batt_level_list[i]
      batt_level_avg = int(batt_level_avg/len(batt_level_list))



    emergency = state_json['emergency']
    pos_now_x = round(state_json['pose']['x'],3)
    pos_now_y = round(state_json['pose']['y'],3)

    # Check if mower is stuck at same pos
    same_pos_time = 0
    if ( (pos_now_x) > (last_pos['x'] - 0.5 ) and (pos_now_x) < ( last_pos['x'] + 0.5 ) ) and \
        ( (pos_now_y) > (last_pos['y'] - 0.5 ) and (pos_now_y) < ( last_pos['y'] + 0.5 ) ):
        same_pos_time = timestamp_now - last_pos['timestamp']
    else:
        last_pos = {'x': pos_now_x, 'y': pos_now_y, 'timestamp': timestamp_now }

    docked = False
    if int(pos_now_x) == 0 and int(pos_now_y) == 0: # When docked, the state_json doesn't contain pos...
        docked = True
    if batt_charge > 12:
        docked = True

    stuck = False
    if same_pos_time > 600 and not docked:
      stuck = True

    print("State:",state_json['current_state'],", Docked:",docked,", Emergency:",emergency,", Pos:",pos_now_x,",",pos_now_y,", Batt:",batt_level_avg,"%, Batt_charge:",batt_charge,"V, Schedule allows mowing:",mowing_time(),", temp:",get_cpu_temp(), "same pos time:",same_pos_time,"stuck:",stuck)

    if not mowing_time() and not docked:
      print("Not mowing time, docking...")
      ret = mower_dock(state_json['current_state'])
    else:
      # when docking although mowing should take place...enter emergency, then the mower will go to idle after a while
      # sometimes the mower never gets to idle, stuck in emergency which will lead to the battery being drained
      if state_json['current_state'] == "DOCKING" and config['mower']['abort_docking']:
        if batt_level_avg > config['mower']['battery_low'] + 20 and mowing_time(): # Not a true SoC, so when motor goes off battery goes up -> hysteresis
          if emergency == 0:
            print("Why are you docking, battery is good, start mowing....will enter emergency")
            time.sleep(10)
            ret = mower_enter_emergency()
          else: # In emergency
            if same_pos_time > 120:
              print("OK, you refuse to go into idle mode. Let's dock instead")
              ret = mower_exit_emergency()
            else:
              print("Waiting for idle state, in emergency mode")
        else:
          if emergency == 1:
            ret = mower_exit_emergency()


      if state_json['current_state'] == "PAUSED" and emergency == 1:
        print("Paused and in emergency, exiting emergency...")
        ret = mower_exit_emergency()
        ret = mower_continue()


      # Start mowing when batt is charged and schedule allows
      if state_json['current_state'] == "IDLE" and docked:
        if previous_state == "DOCKING":
          if batt_level_avg > config['mower']['battery_low'] + 30 and mowing_time():
            if emergency == 1:
              ret = mower_exit_emergency()
            ret = mower_continue()
        if batt_level_avg > config['mower']['battery_high'] and mowing_time():
          print("Can't sleep all day, start mowing....")
          ret = mower_continue()

      # Stop mowing when schedule says stop
      if state_json['current_state'] == "MOWING" and not mowing_time():
        ret = mower_dock(state_json['current_state'])

      # Stop mowing when battery is low
      if state_json['current_state'] == "MOWING" and batt_level < config['mower']['battery_low']:
        ret = mower_dock(state_json['current_state'])

      # Take some action if the mower just idles when it's not docked
      if state_json['current_state'] == "IDLE" and not docked:
        if batt_level_avg > config['mower']['battery_low']:
          print("You lazy bastard, start mowing....")
          ret = mower_continue()
        else:
          # It takes a few seconds after succesful docking for charging voltage to raise
          if same_pos_time > 10: 
            print("How hard is it to dock, really?")
            ret = mower_dock(state_json['current_state'])

    if previous_state != state_json['current_state']:
      previous_state = state_json['current_state']

    lat_new,long_new = translate_latlong(float(config['base_lat']),float(config['base_long']),pos_now_y,pos_now_x)

    mower_data = {'state':state_json['current_state'], 'emergency':emergency, 'pos_x':float(lat_new) , 'pos_y': float(long_new), 'mowing_allowed':mowing_time(), 'last_update':timestamp_now, 'stuck':stuck, 'battery':batt_level}
    rc = publish(mqtt_client_pub, mower_data, "mower/" + str(config['mower']['mower_num']))

def translate_latlong(lat,long,lat_translation_meters,long_translation_meters):
    ''' method to move any lat,long point by provided meters in lat and long direction.
    params :
        lat,long: lattitude and longitude in degrees as decimal values, e.g. 37.43609517497065, -122.17226450150885
        lat_translation_meters: movement of point in meters in lattitude direction.
                                positive value: up move, negative value: down move
        long_translation_meters: movement of point in meters in longitude direction.
                                positive value: left move, negative value: right move
        '''
    earth_radius = 6378.137

    # Calculate top, which is lat_translation_meters above
    m_lat = (1 / ((2 * math.pi / 360) * earth_radius)) / 1000;  
    lat_new = lat + (lat_translation_meters * m_lat)

    # Calculate right, which is long_translation_meters right
    m_long = (1 / ((2 * math.pi / 360) * earth_radius)) / 1000;  # 1 meter in degree
    long_new = long + (long_translation_meters * m_long) / math.cos(lat * (math.pi / 180));

    return lat_new,long_new


def mqtt_on_publish(mqtt_client, userdata, result):
  pass

def mqtt_on_message(mqtt_client,userdata, message,tmp=None):
    global v_charge
    global state_json

#    print(" Received message " + str(message.payload)
#        + " on topic '" + message.topic
#        + "' with QoS " + str(message.qos))

    if message.topic == "robot_state/json":
      state_json = json.loads(message.payload.decode('utf-8'))
      ret = check_mower()
    if message.topic == "sensors/om_v_charge/data":
      v_charge = str(message.payload.decode('utf-8'))

def publish(mqtt_client, pub_var, topic):
  global config

  json_string = json.dumps(pub_var)
  ret = mqtt_client.publish(topic, json_string)

def get_cpu_temp() -> float:
      output = int(subprocess.check_output("cat /sys/class/thermal/thermal_zone0/temp 2>&1", shell=True))
      cpu_temp = float(output/1000)
      return cpu_temp

def main():

    global mqtt_client2,mqtt_client_pub
    global keep_on_running
    global config
    global previous_state
    global v_charge
    global state_json
    global cpu_temp_pi
    global last_pos

    last_pos = {'x':0, 'y':0, 'timestamp':int(time.time())}

    keep_on_running = 1
    cpu_temp_pi = 0

    v_charge = ""
    state_json = {}

    config = read_config_file("mower_watch.yml")

    previous_state = ""

    random_string = str(binascii.b2a_hex(os.urandom(8)))
    mqtt_smarthome_broker = config['mqtt_smarthome_broker']
    mqtt_smarthome_port = config['mqtt_smarthome_port']
    mqtt_smarthome_username = config['mqtt_smarthome_username']
    mqtt_smarthome_password = config['mqtt_smarthome_password']
    # this mqtt client will publish
    mqtt_client_pub = paho.Client(client_id="mower" + str(config['mower']['mower_num']) + random_string, transport="tcp", protocol=paho.MQTTv311,
                              clean_session=True)
    mqtt_client_pub.connected_flag=False
    mqtt_client_pub.on_publish = mqtt_on_publish
    mqtt_client_pub.on_connect = mqtt_on_connect
    mqtt_client_pub.username_pw_set(mqtt_smarthome_username, mqtt_smarthome_password)
    mqtt_client_pub.connect(mqtt_smarthome_broker, mqtt_smarthome_port, keepalive=60)
    mqtt_client_pub.loop_start()

    # this mqtt client will subscribe
    mqtt_client2 = paho.Client(client_id="idle_watch",transport="tcp",protocol=paho.MQTTv311,clean_session=True)  
    mqtt_client2.on_subscribe = mqtt_on_subscribe
    mqtt_client2.on_connect = mqtt_on_connect
    mqtt_client2.on_message = mqtt_on_message
    #mqtt_client2.username_pw_set(mqtt_username, mqtt_password)
    mqtt_client2.connect("127.0.0.1",1883,keepalive=60)
    mqtt_client2.loop_forever()

    while keep_on_running:
      cpu_temp_pi = get_cpu_temp()
      time.sleep(1)



if __name__ == "__main__":
    main()
