#!/bin/bash

while true
do
  now=`date`
  test_host=`netstat -nr | grep "UG" | awk '{ print $2}' | xargs ping -q -w 1 -c 1 | grep "received" | awk '{ print $4 }'`
  if [ "$test_host" == "0" ] || [ -z "$test_host" ] ;
  then
    echo "$now - restarting wlan"
    /usr/sbin/wpa_cli -i wlan0 reconfigure
    sleep 60
  else
    echo "$now - Network ok"
  fi
  sleep 30
done
