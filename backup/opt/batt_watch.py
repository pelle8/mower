
#!/usr/bin/env python

# 2023 Per Carlen
#
#  test sub with: $ mosquitto_sub -h localhost -t sensors/om_v_battery/data
#
#

import os
import time
import json
import yaml
import paho.mqtt.client as paho
import syslog

def mqtt_on_subscribe(client, userdata, mid, granted_qos):
    pass


def mqtt_on_connect(client, userdata, flags, rc):
    if rc != 0:
      print("MQTT connection problem")
      client.connected_flag=False
    else:
      print("MQTT client connected:" + str(client))
      client.connected_flag=True



def mqtt_on_message(mqtt_client,userdata, message,tmp=None):
    global keep_on_running

#    print(" Received message " + str(message.payload)
#        + " on topic '" + message.topic
#        + "' with QoS " + str(message.qos))
    if message.topic == "sensors/om_v_battery/data":
      u_batt = float(message.payload.decode('utf-8'))
      print("Ubat:",u_batt)
      keep_on_running = 0
      if u_batt < 20:
        syslog.syslog('Shutting down, low voltage:' + str(u_batt))
        print("shutting down, low voltage")
        os.system('init 0')
      else:
        syslog.syslog('Voltage ok, ' + str(u_batt))

def main():
    global mqtt_client1
    global keep_on_running

    keep_on_running = 1

    # this mqtt client will subscribe
    mqtt_client2 = paho.Client(client_id="logic_sub",transport="tcp",protocol=paho.MQTTv311,clean_session=True)  
    mqtt_client2.on_subscribe = mqtt_on_subscribe
    mqtt_client2.on_connect = mqtt_on_connect
    mqtt_client2.on_message = mqtt_on_message
    #mqtt_client2.username_pw_set(mqtt_username, mqtt_password)
    mqtt_client2.connect("127.0.0.1",1883,keepalive=60)
    mqtt_client2.subscribe("robot_state/json",2)
    mqtt_client2.subscribe("sensors/om_v_battery/data",2)
    mqtt_client2.loop_start()

    while keep_on_running:
      time.sleep(1)


if __name__ == "__main__":
    main()
