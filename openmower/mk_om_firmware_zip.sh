#!/bin/sh

# Making a firmware.zip for openmower, to be flashed through gui

rm -rf firmware
mkdir -p firmware/0_13_X
find /home/pc/tmp/gitlab/OpenMower/Firmware/LowLevel/.pio/build -iregex ".*\.elf" -exec cp {} ./firmware/0_13_X \; 
find /home/pc/tmp/gitlab/OpenMower/Firmware/LowLevel/.pio/build -iregex ".*\.uf2" -exec cp {} ./firmware/0_13_X \; 
zip firmware.zip -r ./firmware/
scp firmware.zip pelle@192.168.2.208:/var/www/html
