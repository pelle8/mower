# mower

Openmower and mowgli


## BOM

- Biltema RM1000
- RTK base + rover: simpleRTK2B – Starter Kit MR (Ardusimple)
- DC-DC converter (Meanwell NID65-5 DCDC) - 10.5-53Vdc; Output 5Vdc at 6.5A
- Raspberry Pi 4
- ST Link


## Positioning - RTK

- Configuration with U-center, or pygpsclient. Can be done over usbip (config ok, not upgrade)
- Configuration-files on: https://www.ardusimple.com/configuration-files/
- To save config in flash, with U-center: Messages view, UBX-CFG-CFG, save, send.
- Upgrade the flash if it's old.


### LEDs

- Base: No RTK will always be on
- Base: GPS>XBEE should blink
- Rover: No RTK should be off, will be on initially and blink when RTK is received from base
- Rover: XBEE>GPS should blink


### RTK base
- Fix position, either use survey-in mode or external rtkbase on rpi (collect 24hrs, upload to https://webapp.csrs-scrs.nrcan-rncan.gc.ca/geod/tools-outils/ppp.php). Shouldn't be too picky for the mower since everything is relative, we don't need the coordiates themselves to be spot on.
- When used with "rtkbase", the base will be re-configured without XBEE. So after collecting logs, the base needs to be re-configured as a base.
- On POWER+GPS, with 115200, gps data is printed.

#### Survey-in/Fix
- With rktbase, put TMODE3 in disabled (UBX-CFG-TMODE3)
- With fix received, put the fixed postion in TMODE3


NRCAN:
The estimated coordinates ITRF20 2023-09-09 for the 2023-09-09-Your_mount_name_nrcan.obs RINEX file are as follows:

Latitude	N59° 44' 11.2906"	± 0.002 m (95%)
Longitude	E17° 42' 46.0552"	± 0.001 m (95%)
Ellipsoidal Height	56.040 m	± 0.005 m (95%)
[59.73646962,17.71279310,56.040]

WGS84 lantmäteriet: 59°44'11.3"N 17°42'46"E <> 59.736469 , 17.712793, 24 m ö.h.


### RTK rover
- On POWER+GPS, with 115200, gps data is printed.
- On POWER+XBEE, with 115200, RTCM data (from base) is printed.
- Configure NAV-PVT message on USB port, mowgli likes that. UBX+NMEA+RTCM3 out on port.


## RM1000 and related hardware

- Open up, hook up ST-Link
- Backup firmware for panel and main board
- Wire the internals (DC/DC, IMU, RPi....)


### Backup firmware

- sudo openocd -f yardforce500.cfg
- telnet 127.0.0.1 4444

- Following (mainly) https://github.com/cloudn1ne/Mowgli/tree/main/stm32/mainboard_firmware
- Download openocd, compile and install
- Attach dongle to j9
- Run `$ sudo openocd -f yardforce500.cfg -c "init" -c "reset halt"   -c " dump_image firmware_backup_20240729.bin 0x08000000 0x40000 -c "reset" -c shutdown`
- If you get this: "Warn : UNEXPECTED idcode: 0x2ba01477", add "set CPUTAPID 0x2ba01477" to /usr/local/share/openocd/scripts/target/stm32f1x.cfg. (This is apparently a Chinese fake STM).
- Now you should have a firmware_backup.bin, which in my case is 262144 bytes

### Restore firmware
- `$ sudo openocd -f yardforce500.cfg  -c " program "firmware-20240729.bin" 0x08000000 verify reset; shutdown;"`
- `$ sudo openocd -f yardforce500.cfg  -c " program "firmware.bin" 0x08000000 verify reset; shutdown;"`
program rm1000_backup_20230903.bin 0x08000000 verify reset
program rm1000_display_20230904.bin 0x08000000 verify reset

2023 version: program firmware_backup_20240729.bin 0x08000000 verify reset


openocd -f board/stm32f3discovery.cfg \
	-c "program firmware-20240729-YF500.elf  verify reset exit"

#### yardforce500.cfg:

```
source [find interface/stlink.cfg]
source [find target/stm32f4x.cfg]
transport select hla_swd
```


### Compile/flashing firmware

- For me, the setup in om-gui didn't work (got a seg fault while compiling)
- Check main board rev, mine is 6.1. At the moment seems there is a fork supporting this: https://github.com/jeremysalwen/Mowgli.git (branch: yardforce-500b)
- Compile with platform io in vscode
- Flash firmware on main board and panel, using, openocd `/opt/firmware# openocd -f yardforce500.cfg -c "program firmware.elf verify reset exit"`


### Resetting usb without reboot

```
root@mower-pi:~# lsusb 
Bus 004 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub
Bus 003 Device 005: ID 1546:01a9 U-Blox AG u-blox GNSS receiver
Bus 003 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
Bus 002 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub
Bus 001 Device 004: ID 0483:5740 STMicroelectronics Virtual COM Port
Bus 001 Device 005: ID 0483:3748 STMicroelectronics ST-LINK/V2
Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
root@mower-pi:~# usbreset /dev/bus/usb/001/004 
```


### Wiring

![J18](./images/j18.png "J18")

![Wiring](./images/Wiring.jpg "Wiring (too good, couldn't avoid stealing it from the Discord server")
J18: pin 1 to the right, pin4=sda, pin5=scl


## Get the containers

- Clone https://github.com/cedbossneo/mowgli-docker/
- Modify config files
-- wheel ticks, measure
-- distance...
- Go for it


## Area recording

- Hook up Xbox One controller
- Choose Area recording in the gui (if not already in that state)
- Press A and move the leftmost pad to move the mower
- Press B to start polygon, and B again to close polygon. Y + "plus-pad"-UP for navigation and Y + "plus-pad"-DOWN for working area.
- To record docking vector, place the mower about 2 meters from the station with front against station. Press X. Move mower forward until front wheels touches the "floor" of the station. Press X again. Done. I had to move the station a few centimeters after the procedure for successful docking.


## Good for troubleshooting etc

### ROS

- Printing topics (gps pos): `docker exec -it mowgli-openmower /openmower_entrypoint.sh rostopic echo --clear -w 5 /xbot_driver_gps/xb_pose`

```
/xbot_driver_gps/xb_pose
/mower/status
```


### USBIP (linking a connected rtk board to another computer)

To simplify in place configuration, on a computer with U-center....

```
On RPi:
usbip list -l
usbip bind --busid=1-1.1.2
usbipd


On PC:
# usbip attach -r 192.168.2.111 -b 1-1.1.2
...do some work
# usbip detach -p 00
```


## Commands

These are mentioned...
```
/gui/mower_logic/current_state
/gui/mower/status
/gui/xbot_positioning/xb_pose
/gui/imu/data_raw
/gui/mower/wheel_ticks
/gui/xbot_monitoring/map
/gui/slic3r_coverage_planner/path_marker_array
/gui/move_base_flex/FTCPlanner
/gui/mowing_path
Available commands :

/gui/call/mower_service/high_level_control
/gui/call/mower_service/emergency
/gui/call/mower_logic/set_parameters
/gui/call/mower_service/mow_enabled
/gui/call/mower_service/start_in_area
```

## MQTT

```
mosquitto_sub -h localhost -t sensors/om_gps_accuracy/data
0.020889
0.020889

 mosquitto_sub -h localhost -t robot_state/json
{"battery_percentage":0.2710132598876953,"current_action_progress":0.0,"current_state":"MOWING","current_sub_state":"","emergency":0,"gps_percentage":0.8955538272857666,"is_charging":0,"pose":{"heading":164.47166262104486,"heading_accuracy":0.009999999776482582,"heading_valid":1,"pos_accuracy":0.02088923193514347,"x":-14.341730024804797,"y":-38.311086040285105}}

mosquitto_sub -h localhost -t sensors/om_v_battery/data
24.140234
24.132957
```

## Extras
 
- Using rc.local to start things and adding things like a stable IP for openmower and NAT:ing from wlan0
- Using a monitoring service for battery voltage, that shuts down the RPi before draining the battery.
- Using a controller-script that reads a schedule and keeps the mower going within mowing window and docks if outside...etc


## API

Continue:

POST /api/openmower/call/mower_logic HTTP/1.1
Accept: */*
Accept-Encoding: gzip, deflate
Accept-Language: sv-SE,sv;q=0.9,en-US;q=0.8,en;q=0.7
Cache-Control: no-cache
Connection: keep-alive
Content-Length: 67
Content-Type: application/json
Host: 192.168.2.179:4006
Origin: http://192.168.2.179:4006
Pragma: no-cache
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/112.0.0.0 Safari/537.36

{"Config":{"Bools":[{"Name":"manual_pause_mowing","Value":false}]}}

-----------

POST /api/openmower/call/high_level_control HTTP/1.1
Accept: */*
Accept-Encoding: gzip, deflate
Accept-Language: sv-SE,sv;q=0.9,en-US;q=0.8,en;q=0.7
Cache-Control: no-cache
Connection: keep-alive
Content-Length: 13
Content-Type: application/json
Host: 192.168.2.179:4006
Origin: http://192.168.2.179:4006
Pragma: no-cache
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/112.0.0.0 Safari/537.36

{"Command":1}

-------------

Emergency on:

POST /api/openmower/call/emergency HTTP/1.1
Accept: */*
Accept-Encoding: gzip, deflate
Accept-Language: sv-SE,sv;q=0.9,en-US;q=0.8,en;q=0.7
Cache-Control: no-cache
Connection: keep-alive
Content-Length: 15
Content-Type: application/json
Host: 192.168.2.179:4006
Origin: http://192.168.2.179:4006
Pragma: no-cache
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/112.0.0.0 Safari/537.36

{"Emergency":1}

-------------

Emergency off:

POST /api/openmower/call/emergency HTTP/1.1
Accept: */*
Accept-Encoding: gzip, deflate
Accept-Language: sv-SE,sv;q=0.9,en-US;q=0.8,en;q=0.7
Cache-Control: no-cache
Connection: keep-alive
Content-Length: 15
Content-Type: application/json
Host: 192.168.2.179:4006
Origin: http://192.168.2.179:4006
Pragma: no-cache
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/112.0.0.0 Safari/537.36

{"Emergency":0}


-------------

Home:

POST /api/openmower/call/high_level_control HTTP/1.1
Accept: */*
Accept-Encoding: gzip, deflate
Accept-Language: sv-SE,sv;q=0.9,en-US;q=0.8,en;q=0.7
Cache-Control: no-cache
Connection: keep-alive
Content-Length: 13
Content-Type: application/json
Host: 192.168.2.179:4006
Origin: http://192.168.2.179:4006
Pragma: no-cache
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/112.0.0.0 Safari/537.36

{"Command":2}


# DEBUG:
Comment out a few lines in docker-compose.yaml

# Tile-server
https://github.com/2m/openmower-map-tiles

docker run -ti --rm   -v /home/pc/tmp/maps/datasets/project/odm_orthophoto/odm_orthophoto.tif:/tiles/map.tif   -p 5000:5000   -p 5010:5010   ghcr.io/2m/openmower-map-tiles:main

http://localhost:5000/rgb/18/143969/76509.png?r=red&g=green&b=blue


## Convert images to orto:
https://wiki.openmower.de/index.php?title=Modify_map_using_drone_footage

https://github.com/OpenDroneMap/WebODM/

git clone https://github.com/OpenDroneMap/WebODM --config core.autocrlf=input --depth 1
cd WebODM
./webodm.sh start 

or:

https://github.com/2m/openmower-map-tiles

podman run -it --rm -v /opt/odm/datasets:/datasets 09fe808e77ad --project-path /datasets project --pc-quality ultra --feature-quality ultra --matcher-type bruteforce --orthophoto-resolution 1

40m alt: 5m<>355px , 1px<>1.4cm

qgis

- Add plugin: AnotherDXFImporter
- New project, "AD"-button (plugin). Choose file from odm
- Add OpenStreetMap XYZ Tile
- Move/scale/rotate to position
- Export raster-button 
- Layer-georeferencer
- Open raster (saved in export)
- "Play", two points, "play" to save
- Layer, export, save as (geotiff)



## Optimow 10 with Vermut openmower kit

### DC-DC

- Adjust voltage and current limit to battery

### Flashing pico

- Open up OpenMower-repo i vs code, use platform io
- Build
- Either attach a usb to pico and mount it as a disk (when pushing white button and power cycle), or use openmower-gui (check a valid firmware.zip for content)


### Hallsensors

The logic levels are inverters in relation to what om wants. Added a PR for this that adds the possibility to invert these values.

### Motors

Change x-esc config for the motors.


#### Wiring

LM,RM:

```
Yel - U    1 8  Pur - Hall, U
Blu - V    2 7  Gra - Hall, V
Whi - W    3 6  Gre - Hall, W
Bla - GND  4 5  Red - 5V
```

CM:

```
Red - 5V   1 10 
Gre - U    2 9 
Whi - V    3 8  Pur - Hall, U
Blu - W    4 7  Yel - Hall, V
Bla - GND  5 6  Gra - Hall, W
```


J1:

```
ML_W   12 1   LM_H_U
ML_V   13 2   LM_H_V
ML_U   14 3   LM_H_W
GND    15 4   5V
MC_W   16 5   x
MC_V   17 6   GND
MC_U   18 7   MC_TEMP (NTC)
MR_W   19 8   RM_H_U
MR_V   20 9   RM_H_V
MR_U   21 10  RM_H_W
GND    22 11  5V
```

J4/J10:

```
5 GND
4 LM_H_U
3 LM_H_V
2 LM_H_W
1 5V
```



#### Find ttyAMA numbering

Testscript for finding AMA-numbering:

```
#!/bin/sh
sudo stty -F /dev/ttyAMA0 115200
sudo stty -F /dev/ttyAMA1 115200
sudo stty -F /dev/ttyAMA2 115200
sudo stty -F /dev/ttyAMA3 115200
sudo stty -F /dev/ttyAMA4 115200

while true; do
  sudo sh -c "echo 0 > /dev/ttyAMA0"
  sudo sh -c "echo 1 > /dev/ttyAMA1"
  sudo sh -c "echo 2 > /dev/ttyAMA2"
  sudo sh -c "echo 3 > /dev/ttyAMA3"
  sudo sh -c "echo 4 > /dev/ttyAMA4"
  sleep 1
done
```

Findings:

- p8 - AMA0 (UART0) - pico
- p24 - AMA3 (MC)
- p32 - AMA4 (ML)
- p07 - AMA2 (MR)


#### VESC tool

- Attach battery (+24V on left side of Fuse F4)

On rpi:

- Before running vesc-tool, run `stty -F /dev/ttyAMAX 115200` set baudrate, will not find interface otherwise
- Redirect tcp to ttyAMAX, `socat tcp-listen:65102,reuseaddr,fork file:/dev/ttyAMAX,raw`
- Start vesc tool: `./vesc-tool_6.02`
- Connection (tcp)...then adjust config, change number of cells etc, test by pressing small play-button: D:0,5, I:0,1, w:100, P:0,00, IB:3.00, HB:3,00 

#### Motors

- Adjust voltage (number of cells)
- Motor Settings-FOC-General: Detect and calculate (may need to tweak start params to get the motor spinning) - Apply
- Motor Settings-FOC-Hall Sensors: Detect Hall Sensors - Apply
- Write motor configuration

24.6 gui <> 24.6 real
21.65 gui <> 21.6 real
21.33 gui <> 21.25 real

### Plotjuggler

Install: `sudo snap install plotjuggler-ros`

På maskin som kör om:
```
Stoppa in `export ROS_IP=192.168.2.180` i den fil (/boot/openmower/mower_config.txt) som används av containern med om.
Starta om openmower.service.
```

På maskin som kör plotjuggler:
```
export ROS_MASTER_URI=http://192.168.2.180:11311
plotjuggler-ro
```

För att plotta x/y: välj båda källorna, dra med höger musknapp nertryckt.

